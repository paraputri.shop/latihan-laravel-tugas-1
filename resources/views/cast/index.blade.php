@extends('layout.master')
@section('title')
Halaman List Caster
@endsection
@section('content')
{{-- copy paste TABLE dr boostrap 4 --}}

<a href="/cast/create" class="btn btn-primary mb-3">Tambah Caster</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item -> nama}}</td>
                <td>{{$item -> umur}}</td>
                <td>{{$item -> bio}}</td>
                <td>            
                    <form class="mt-2" action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse

    </tbody>
  </table>

@endsection