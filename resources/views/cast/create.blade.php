@extends('layout.master')
@section('title')
Halaman Tambah Caster
@endsection
@section('content')
{{-- copy paste FORM dr boostrap 4 --}}
<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" cols="30" rows="5" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>


@endsection