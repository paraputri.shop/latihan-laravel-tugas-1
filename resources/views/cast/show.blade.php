@extends('layout.master')
@section('title')
Halaman Detail Caster
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('delete')
    <a href="/cast" class="btn btn-light btn-sm">Kembali</a>
    <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
    <input type="submit" value="delete" class="btn btn-danger btn-sm">
</form>
    <br>
    <h4>Nama:<br>{{$cast->nama}}</h4>
    <h4>Umur:<br>{{$cast->umur}}</h4>
    <h4>Biodata:<br>{{$cast->bio}}</h4>

@endsection