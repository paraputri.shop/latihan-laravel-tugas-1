@extends('layout.master')
@section('title')
Halaman Pendaftaran
@endsection
@section('content')
    <h2>Buat Account Baru</h2>
    <h3>Form Sign Up</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label>First Name</label><br>
            <input type="text" name="fname"><br><br>

            <label>Last Name</label><br>
            <input type="text" name="lname"><br><br>

            <label>Gender</label><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br><br>

            <label>Nationality</label><br>
            <select>
                <option value="1">Indonesia</option>
                <option value="2">Amerika</option>
                <option value="3">Inggris</option>
            </select><br><br>

            <label>Language Spoken</label><br>
            <input type="checkbox" name="lang">Bahasa Indonesia<br>
            <input type="checkbox" name="lang">English<br>
            <input type="checkbox" name="lang">Other<br><br>

            <label>Bio</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br><br>

            <input type="submit" value="Sign Up">
        </form>
@endsection
