<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@daftar');
Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-tables', 'IndexController@table');

// CRUD tabel Cast
//create
Route::get('/cast/create', 'CastController@create'); // mengarah ke form tambah Cast
Route::post('/cast', 'CastController@store'); // menyimpan data ke DB

//read
Route::get('/cast', 'CastController@index');  // ambil data ke DB terus ditampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); // menampilkan detail data

//update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // untuk diarahin ke form edit
Route::put('/cast/{cast_id}', 'CastController@update');

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');  // untuk menghapus data berdasarkan id


