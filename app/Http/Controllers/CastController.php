<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cast;  // ditambahkan untuk simpan/store data ke DB

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
 
        return view('cast.index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //1. buat validasi data inputan
        $request->validate([
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Ketik "Nama" untuk melanjutkan',
                'umur.required'  => 'Ketik "Umur" untuk melanjutkan',
                'bio.required'  => '"Biodata" tidak boleh kosong',
            ]
        );

        //2. simpan data ke DB
        $cast = new Cast;
 
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
 
        $cast->save();

        //3. arahkan ke halaman cast
        return redirect('/cast'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::find($id);

        return view('cast.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id);

        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            //1. buat validasi data inputan
            $request->validate([
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Ketik "Nama" untuk melanjutkan',
                'umur.required'  => 'Ketik "Umur" untuk melanjutkan',
                'bio.required'  => '"Biodata" tidak boleh kosong',
            ]
        );

        //2. simpan data ke DB
        $cast = Cast::find($id);
 
        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];
         
        $cast->save();

        //3. arahkan ke halaman cast
        return redirect('/cast'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);
 
        $cast->delete();

        //3. arahkan ke halaman cast
        return redirect('/cast'); 

        }
}
