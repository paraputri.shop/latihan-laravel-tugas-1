<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('page.register');
    }

    public function kirim(Request $request){
        $fnama = $request["fname"];
        $lnama = $request["lname"];

        return view('page.welcome', compact("fnama","lnama"));
    }
}
